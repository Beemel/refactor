package refactored;

public class Alarm {
    private final double LowPressureThreshold = 17;
    private final double HighPressureThreshold = 21;

    //Sensor renamed from sensor to valueFromSensor, to make more sense(or)
    Sensor valueFromSensor = new Sensor();
    boolean alarmOn = false;

    public void check(){
        double psiPressureValue = valueFromSensor.sensorReading();

        if(psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue){
            alarmOn = true;
        }
    }
    public boolean isAlarmOn(){
        return alarmOn;
    }
}
