package refactored;
// Sensor is now optimized and do one thing WELL!
import java.util.Random;

public class Sensor {
    public static final double OFFSET = 16;

    //popNextPressurePsiValue has been omitted since it was deemed unnecessary
    //Instead the final calculation of the pressure is handled by the samplePressure
    //method. The name samplePressure has been renamed to SensorReading
    //which I think is a more appropriate naming. Since that what it is.

    public double sensorReading() {
        Random basicRandomNumbersGenerator = new Random();
        double pressureTelemetryValue = 6 * basicRandomNumbersGenerator.nextDouble() * basicRandomNumbersGenerator.nextDouble();
        return OFFSET + pressureTelemetryValue;
    }
}
