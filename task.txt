Task: Refactor legacy code
Scenario: Tire pressure monitoring system
This is a simple system where there is an refactored.Alarm class designed to monitor
the tire pressure and set an alarm if the pressure falls outside of the
expected range. The sensor class provided simulates the behavior of a
real tire sensor, providing random but realistic values.

Look through the given classes and identify which SOLID principles they
are breaking. Identify the line(s) and principle(s). You are then to fix the
classes to be more SOLID. There will be one or two violations – so not
major refactors just a line or and whatever additions you need to make
(other classes, interfaces, etc.).
The important thing is to not change the public facing API – meaning that
the classes still need to produce the same output, so no functionality
should be changed. You do not have to demonstrate their use – nothing
needs to be written in main(). You are just changing how the classes
interact.
The code for the scenario can be found on the Moodle site.
Note: There are two packages:
The first is a scenario where you will find the classes. You are to comment
where you think a SOLID principle is broken and explain why and what the
principle was.
The second is where your solution will be located. Copy the original
classes there with your changes and additions included. There should be
comments showing what you changed where possible.